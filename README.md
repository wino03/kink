# KinK - Helm Charts Deployment Tester


The purpose of this repository is to provide a place for maintaining and contributing your Helm Charts, with CI processes in place for test your charts deployment to the "ephemeral" KinK Cluster.


## Repository Structure

```
├── charts
│   ├── ingress-nginx
│   │   ├── templates
│   │   │   ├── …
│   │   │   └─ daemonset.yaml
│   │   │
│   │   ├── Chart.yaml
│   │   ├── kink-values.yaml
│   │   └── values.yaml
│   │
│   └── traefik
│       ├── crds
│       │   ├── …
│       │   └── traefikservices.yaml
│       │
│       ├── templates
│       │   ├── …
│       │   └── service.yaml
│       │
│       ├── Chart.yaml
│       ├── kink-values.yaml
│       └── values.yaml
│
├── helpers
│   │
│   ├── images
│   │   ├── ci-tools
│   │   │   └── Dockerfile
│   │   │
│   │   └── k3s
│   │       ├── …
│   │       └── Dockerfile
│   │
│   ├── manifests
│   │   ├── master.yaml
│   │   └── nodes.yaml
│   │
│   ├── runners
│   │   └── gitlab-runners-helm-config-values.yaml
│   │
│   └── scripts
│       ├── ci.gitlab.sh
│       └── ci.shell.sh
│
└── README.md
```

The directory in this repository are organized into two folders:

* `charts`
* `helpers`

`charts` directory contains the source for your charts, each chart should have `kink-values.yaml` that will be used by CI pipeline in helm deployment to KinK cluster. The file also can be used to define some of KinK node resources.

`helpers` consist of several source of material used by GitLab CI:

* `images`, this folder contains ci-tools and modified k3s's Dockerfile
* `manifests`, this folder hold Kubernetes (parent) Service and Pod YAML Object
* `runners`, the values YAML file inside this folder were used to deploy gitlab/gitlab-runner to the Kubernetes parent cluster
* `scripts`, several bash script function used by GitLab CI stages.
