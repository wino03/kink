#!/bin/bash

# only set -e when it's not interactive
[[ ! $- =~ i ]] && set -e

ci_gitlab_changedfiles() {
    local _path_pattern=${1}

    export CI_TARGET_PARENT_SHA=${CI_COMMIT_SHA}
    export CI_TARGET_BRANCH=${CI_DEFAULT_BRANCH}

    case ${CI_PIPELINE_SOURCE} in
        merge_request_event)
            CI_TARGET_PARENT_SHA=$(git ls-remote origin ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME} -q | awk '{ print $1 }')
            ;;

        push)
            [[ "${CI_DEFAULT_BRANCH}" != "${CI_COMMIT_BRANCH}" ]] && CI_TARGET_BRANCH=${CI_COMMIT_BRANCH}
            CI_TARGET_PARENT_SHA=$(git ls-remote origin ${CI_TARGET_BRANCH} -q | awk '{ print $1 }')
            ;;

        *)
            :
            ;;
    esac

    [[ ! -a "${_path_pattern}" ]] && _path_pattern=""
    git diff-tree --diff-filter=dux --no-commit-id --name-only -r ${CI_TARGET_PARENT_SHA} -r ${CI_COMMIT_SHA} ${_path_pattern}
}
